# Configure postfix to use TLS communication for a specific relay host 
We must tell postfix to use SSL communication for a specific relay host using the following settings in `/etc/postfix/main.cf` file. 

```
relayhost = [replace_with_relay_ip_orfqdn:port]
smtp_tls_policy_maps = hash:/etc/postfix/tls_policy
```

In the `/etc/postfix/tls_policy` file add the following entry. 
```
[replace_with_relay_ip_orfqdn:port] encrypt
```

For more options refer to the the upstream [article](http://www.postfix.org/postconf.5.html#smtp_tls_policy_maps) 


Execute the following command to create a hash DB file from `/etc/postfix/tls_policy` file.
```
  # postmap hash:/etc/postfix/tls_policy
 ```

Now restart the postfix service and check email using mailx command. 


# Debug a postfix mail server

Append one or more "-v" options to selected daemon definitions in   /etc/postfix/master.cf   and type "postfix reload". This will cause a lot of activity to be logged to the syslog daemon. For example, to make the Postfix SMTP server process more verbose:

```
/etc/postfix/master.cf:
    smtp      inet  n       -       n       -       -       smtpd -v
```

### Postfix can attach a call tracer whenever a daemon process starts. Call tracers come in several kinds.

System call tracers such as trace, truss, strace, or ktrace. These show the communication between the process and the kernel.

Library call tracers such as sotruss and ltrace. These show calls of library routines, and give a better idea of what is going on within the process.

Append a -D option to the suspect command in /etc/postfix/master.cf, for example:

```
/etc/postfix/master.cf:
    smtp      inet  n       -       n       -       -       smtpd -D
Edit the debugger_command definition in /etc/postfix/main.cf so that it invokes the call tracer of your choice, for example:

/etc/postfix/main.cf:
    debugger_command =
         PATH=/bin:/usr/bin:/usr/local/bin;
         (truss -p $process_id 2>&1 | logger -p mail.info) & sleep 5
```

For more oprions refer to the upstream [article](http://www.postfix.org/DEBUG_README.html) 

# mailx command to send email to an SMTP server directly using TLS communication or encrypted channel - [article](https://gitlab.com/rameshsahoo11/shared_docs/-/blob/master/Postfix/mailx.md)


