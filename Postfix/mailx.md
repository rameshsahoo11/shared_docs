# mailx command to send email to an SMTP server directly using TLS communication or encrypted channel 

```
echo -e "Email content" | mailx -vvv -s "Email subject - test" \
-S smtp-use-starttls \ 
-S ssl-verify=ignore \
-S smtp=smtp://replace_with_smtp_ip_or_fqdn \
-S nss-config-dir="/etc/pki/nssdb/" \
-S from="from_addr"\
$replace_with_to_addr
```
