1. Generate private key
   ```
   $ openssl genrsa -des3 -out server.key 1024
   ```


2. Remove pass from the private key
```
   $ openssl rsa -in server.key -out server.key.wp 
   $ rm server.key
   $ mv server.key.wp server.key
```

3. Create the certificate for 10000 days
```
   $ openssl req -new -x509 -key server.key -out server.crt -days 10000
```


4. Now verify the certificate
```
   $ openssl x509 -in server.crt  -noout -text
```
