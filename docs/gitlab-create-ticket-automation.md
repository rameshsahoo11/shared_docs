# Automatation proceduere to create a ticket on gitlab.com 


> It is possible to create and manage GitLab tickets using a variety of automation tools. In this document, I will show the procedure to create a GitLab ticket using Linux curl command and a Python program. 

### Prerequisite
- Login to your GitLab account.
- Navigate your **profile** icon on the top right corner of the window  → select **Settings**.
- In the Settings page, navigate **“Access Tokens”** to create a **“Personal access Token”**.
- In the **Name** box, type the name of the token. Eg. **“create ticket”**
- Under the **Scope** section(just below the name box), select the **api** option. 
- Now click on the button **“Create Personal Access Token”** to create the token. 
- **Imp:** Make a note of the **access token** and save it somewhere because you won’t be able to see this later. 
- Make a note of the **project ID**(numerical) by navigating **Projects → Your Projects**. 


Now lets create ticket using **curl** command and a **Python** program:


### Curl command to create a GitLab ticket 
- List the present issues. 
```bash
   $ curl --header "PRIVATE-TOKEN: xxxxxxxxxxxxx" "https://gitlab.com/api/v4/projects/replace_with_project_id/issues"
```
- If the above comand is successful, you will get list of issue like below:

```json
[{"id":73348479,"iid":2,"project_id":21993192,"title":"Program error found with transaction 4556742 on db33221.app33","description":"**Start time:** 26 October 2020, 6:06AM (UTC)  \n**Severity:** high  \n**Service:** db33221.app33  \n**Description:** Urgent attention is needed to fix this issue.  \n**GitLab alert:** https://gitlab.com/rameshsahoo11/study_python/-/alert_management/2/details","state":"opened","created_at":"2020-10-26T06:06:06.425Z","updated_at":"2020-10-26T06:06:06.425Z","closed_at":null,"closed_by":null,"labels":["incident"],"milestone":null,"assignees":[],"author":{"id":4002669,"name":"GitLab Alert Bot","username":"alert-bot","state":"active","avatar_url":"https://secure.gravatar.com/avatar/78d8b26d861d068722cdca55bbe0fc21?s=80\u0026d=identicon","web_url":"https://gitlab.com/alert-bot"},"assignee":null,"user_notes_count":0,"merge_requests_count":0,"upvotes":0,"downvotes":0,"due_date":null,"confidential":false,"discussion_locked":null,"web_url":"https://gitlab.com/rameshsahoo11/study_python/-/issues/2","time_stats":{"time_estimate":0,"total_time_spent":0,"human_time_estimate":null,"human_total_time_spent":null},"task_completion_status":{"count":0,"completed_count":0},"blocking_issues_count":null,"has_tasks":false,"_links":{"self":"https://gitlab.com/api/v4/projects/21993192/issues/2","notes":"https://gitlab.com/api/v4/projects/21993192/issues/2/notes","award_emoji":"https://gitlab.com/api/v4/projects/21993192/issues/2/award_emoji","project":"https://gitlab.com/api/v4/projects/21993192"},"references":{"short":"#2","relative":"#2","full":"rameshsahoo11/study_python#2"},"moved_to_id":null,"health_status":null},{"id":73347678,"iid":1,"project_id":21993192,"title":"Program error found with 11221 transaction","description":"**Start time:** 26 October 2020, 5:27AM (UTC)  \n**Severity:** high  \n**Service:** db2231  \n**Description:** Program error found with 11221 transaction. Needs Urgent attention.  \n**GitLab alert:** https://gitlab.com/rameshsahoo11/study_python/-/alert_management/1/details","state":"opened","created_at":"2020-10-26T05:27:13.853Z","updated_at":"2020-10-26T05:27:13.853Z","closed_at":null,"closed_by":null,"labels":["incident"],"milestone":null,"assignees":[],"author":{"id":4002669,"name":"GitLab Alert Bot","username":"alert-bot","state":"active","avatar_url":"https://secure.gravatar.com/avatar/78d8b26d861d068722cdca55bbe0fc21?s=80\u0026d=identicon","web_url":"https://gitlab.com/alert-bot"},"assignee":null,"user_notes_count":0,"merge_requests_count":0,"upvotes":0,"downvotes":0,"due_date":null,"confidential":false,"discussion_locked":null,"web_url":"https://gitlab.com/rameshsahoo11/study_python/-/issues/1","time_stats":{"time_estimate":0,"total_time_spent":0,"human_time_estimate":null,"human_total_time_spent":null},"task_completion_status":{"count":0,"completed_count":0},"blocking_issues_count":null,"has_tasks":false,"_links":{"self":"https://gitlab.com/api/v4/projects/21993192/issues/1","notes":"https://gitlab.com/api/v4/projects/21993192/issues/1/notes","award_emoji":"https://gitlab.com/api/v4/projects/21993192/issues/1/award_emoji","project":"https://gitlab.com/api/v4/projects/21993192"},"references":{"short":"#1","relative":"#1","full":"rameshsahoo11/study_python#1"},"moved_to_id":null,"health_status":null}]
```

- Create a new issue using the following command.

```bash
$ curl --request POST --data '{"title": "Found a bug with program execution","description": "Fix the Bug ASAP", "labels": "bug"}' --header "Content-Type: application/json" --header "PRIVATE-TOKEN: xxxxx" "https://gitlab.com/api/v4/projects/replace_with_project_id/issues"
```

- You will get a response like below if the above command executes successfully.

```json
{"id":73349936,"iid":3,"project_id":21993192,"title":"Found a bug with program execution","description":"Fix the Bug ASAP","state":"opened","created_at":"2020-10-26T06:49:40.058Z","updated_at":"2020-10-26T06:49:40.058Z","closed_at":null,"closed_by":null,"labels":["bug"],"milestone":null,"assignees":[],"author":{"id":6037594,"name":"Ramesh Sahoo","username":"rameshsahoo11","state":"active","avatar_url":"https://secure.gravatar.com/avatar/4b75697ab3cbc01034f714f3710b8876?s=80\u0026d=identicon","web_url":"https://gitlab.com/rameshsahoo11"},"assignee":null,"user_notes_count":0,"merge_requests_count":0,"upvotes":0,"downvotes":0,"due_date":null,"confidential":false,"discussion_locked":null,"web_url":"https://gitlab.com/rameshsahoo11/study_python/-/issues/3","time_stats":{"time_estimate":0,"total_time_spent":0,"human_time_estimate":null,"human_total_time_spent":null},"task_completion_status":{"count":0,"completed_count":0},"weight":null,"blocking_issues_count":null,"has_tasks":false,"_links":{"self":"https://gitlab.com/api/v4/projects/21993192/issues/3","notes":"https://gitlab.com/api/v4/projects/21993192/issues/3/notes","award_emoji":"https://gitlab.com/api/v4/projects/21993192/issues/3/award_emoji","project":"https://gitlab.com/api/v4/projects/21993192"},"references":{"short":"#3","relative":"#3","full":"rameshsahoo11/study_python#3"},"subscribed":true,"moved_to_id":null,"health_status":null}
```

### Python program to create a GitLab ticket 

- Now, we can use the Python program to create a GitLab ticket. 

```python
import json
import requests
 
def create_alert(data):
    '''
    - Create a gitlab ticket using a valid Access Token
    - Accepts data input as dictionary
    - Payload templates are available at:
      https://docs.gitlab.com/ee/api/issues.html#new-issue
    '''
 
    headers = {"Content-Type": "application/json","PRIVATE-TOKEN": "fo2N9yEyZeMByt5-WJ38"}
    api_url = "https://gitlab.com/api/v4/projects/21993192/issues"
 
    try:
        response = requests.post(api_url, data=json.dumps(data), headers=headers)
        # If the response was successful, no Exception will be raised
        response.raise_for_status()
        print('success')
 
    except HTTPError as http_err:
        print(f'HTTP error occurred with HTTP POST: {http_err}')
 
    except Exception as err:
        print(f'Other error occurred with HTTP POST: {err}')
 
 
if __name__ == "__main__":
 
    data = {
            'title':'Found a bug with program execution',
            'description': 'Urgent attention is needed to fix this issue.',
            'labels': 'bug'
            }
 
    create_alert(data)
```


- Now run the program and verify if a new ticket has been created on GitLab.

```bash
$ python3 gitlab_create_ticket.py
Success
```  

- Now navigate to the “Issues” section in your project to see the new ticket created by your GitLab account. 

#### Reference
https://docs.gitlab.com/ee/api/issues.html#new-issue

