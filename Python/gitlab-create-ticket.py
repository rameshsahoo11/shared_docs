import json
import requests
 
def create_alert(data):
    '''
    - Create a gitlab ticket using a valid Access Token
    - Accepts data input as dictionary
    - Payload templates are available at:
      https://docs.gitlab.com/ee/api/issues.html#new-issue
    '''
 
    headers = {"Content-Type": "application/json","PRIVATE-TOKEN": "xxxxxxxxx"}
    api_url = "https://gitlab.com/api/v4/projects/21993192/issues"
 
    try:
        response = requests.post(api_url, data=json.dumps(data), headers=headers)
        # If the response was successful, no Exception will be raised
        response.raise_for_status()
        print('success')
 
    except HTTPError as http_err:
        print(f'HTTP error occurred with HTTP POST: {http_err}')
 
    except Exception as err:
        print(f'Other error occurred with HTTP POST: {err}')
 
 
if __name__ == "__main__":
 
    data = {
            'title':'Found a bug with program execution',
            'description': 'Urgent attention is needed to fix this issue.',
            'labels': 'bug'
            }
 
    create_alert(data)