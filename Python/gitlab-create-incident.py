import json
import requests
 
def create_alert(data):
    '''
    - Create a GitLab incident using a valid authorization key
    - Accepts data input as a dictionary
    - Payload templates are available at: https://docs.gitlab.com/ee/operations/incident_management/alert_integrations.html#generic-http-endpoint
    '''
 
    headers = {"Content-Type": "application/json","Authorization": "Bearer xxxxxxxxxxx"}
    api_url = "https://gitlab.com/rameshsahoo11/study_python/alerts/notify.json"
 
    try:
        response = requests.post(api_url, data=json.dumps(data), headers=headers)
        # If the response was successful, no Exception will be raised
        response.raise_for_status()
        print('success')
 
    except HTTPError as http_err:
        print(f'HTTP error occurred with HTTP POST: {http_err}')
 
    except Exception as err:
        print(f'Other error occurred with HTTP POST: {err}')
 
 
if __name__ == "__main__":
 
    data = {
            'title':'Program error found with transaction 4556742 on db33221.app33',
            'description': 'Urgent attention is needed to fix this issue.',
            'service': 'db33221.app33',
            'severity': 'high'
            }
 
    create_alert(data)
